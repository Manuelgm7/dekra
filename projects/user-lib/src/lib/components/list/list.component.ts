import { Component, OnInit } from '@angular/core';
import { User } from '../../entities/user';
import { UserManagerService } from '../../services/user-manager.service';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})


export class ListComponent implements OnInit {
  displayedColumns: string[] = [
    'username',
    'name',
    'surnames',
    'email',
    'age',
    'active',
    'lastLogging',
    'actions'
  ];
  dataSource: User[];

  constructor(private userManagerSrv: UserManagerService
  ) { }

  ngOnInit() {
    this.userManagerSrv.getAllUser().subscribe(
      (data: User[]) => {
        this.dataSource = data;
      },
      (error) => {
        console.log(error);
      }
    )
  }

  formatDate(date: number): string{
    const dateAux = new Date(date);
    return dateAux.getDate() + "/" + dateAux.getMonth() + "/" + dateAux.getFullYear() 
  }

  onDelete(id: string){
    this.userManagerSrv.deleteUser(id).subscribe(
      (data: User[]) => {
        this.dataSource = data;
        console.log(`User deleted`);
      },
      (error) => {
        console.log(error);
      }
    )
  }

}