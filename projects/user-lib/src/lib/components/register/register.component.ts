import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { OperationRequest, User } from '../../entities/user';
import { UserManagerService } from '../../services/user-manager.service';
import * as uuid from "uuid";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  title: string;
  email: string;
  isNew: boolean = true;
  oldUser: User;

  registerForm: FormGroup;
  emailRegx = /^(([^<>+()\[\]\\.,;:\s@"-#$%&=]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,3}))$/;
  numberRegx = /^-?(0|[1-9]\d*)?$/;

  constructor(
    private rutaActiva: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private userManagerSrv: UserManagerService
  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username: [null, Validators.required],
      name: [null, Validators.required],
      surnames: [null, Validators.required],
      email: [null, [Validators.required, Validators.pattern(this.emailRegx)]],
      age: [null, [Validators.required, Validators.pattern(this.numberRegx)]],
      password: [null, Validators.required],
    });
    this.email = this.rutaActiva.snapshot.params.email;
    if (this.email != "undefined"){
      this.title = "Edit User";
      this.isNew = false;
      this.userManagerSrv.getAllUserById(this.email).subscribe({
        next: (data: User) => {
          this.oldUser = data;
          console.log("AQUIIII",this.oldUser.username);
          this.registerForm.controls['username'].setValue(this.oldUser.username);
          this.registerForm.controls['name'].setValue(this.oldUser.name);
          this.registerForm.controls['surnames'].setValue(this.oldUser.surnames);
          this.registerForm.controls['email'].setValue(this.oldUser.email);
          this.registerForm.controls['password'].setValue(this.oldUser.password);
          this.registerForm.controls['age'].setValue(this.oldUser.age);
        },
        error: (error) => {
          console.log(error);
        },
      });
    } else {
      this.title = "Register";
      this.registerForm.controls['email'].setValue("");
    }
  }

  submit() {
    if (!this.registerForm.valid) {
      return;
    }
    if (this.isNew){
      const newUser: User = {
        id: uuid.v4(),
        username: this.registerForm.value.username,
        name: this.registerForm.value.name,
        surnames: this.registerForm.value.surnames,
        email: this.registerForm.value.email,
        password: this.registerForm.value.password,
        age: this.registerForm.value.age,
        active: false,
        lastLogging: new Date().getTime(),
        creationDate: new Date().getTime(),
      }
      this.userManagerSrv.createUser(newUser).subscribe({
        next: (data: OperationRequest) => {
          console.log(`User ${newUser.username} created`);
            this.router.navigateByUrl('/list');
        },
        error: (error) => {
          console.log(error);
        },
      });
    } else {
      const editUser: User = {
        id: this.oldUser.id,
        username: this.registerForm.value.username,
        name: this.registerForm.value.name,
        surnames: this.registerForm.value.surnames,
        email: this.registerForm.value.email,
        password: this.registerForm.value.password,
        age: this.registerForm.value.age,
        active: this.oldUser.active,
        lastLogging: new Date().getTime(),
        creationDate: this.oldUser.creationDate,
      }
      this.userManagerSrv.updateUser(editUser).subscribe({
        next: (data: OperationRequest) => {
          console.log(`User ${editUser.username} updated`);
            this.router.navigateByUrl('/list');
        },
        error: (error) => {
          console.log(error);
        },
      });
    }
  }

  onCancel(){
    if (this.isNew){
      this.router.navigateByUrl('/login');
    } else {
      this.router.navigateByUrl('/list');
    }
  }
}