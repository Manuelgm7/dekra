import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { OperationRequest, User } from '../entities/user';
import { UserManagerService } from './user-manager.service';

describe('UserManagerService', () => {
  let service: UserManagerService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserManagerService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(UserManagerService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterAll(() => {
    httpMock.verify();
  })

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getData get data and does a get method', () => {
    const dataTest: User[] = [
      {
        id: "demo",
        username: 'demo',
        name: 'Demo',
        surnames: 'demo',
        email: 'demo@email.com',
        password: 'demo',
        age: '50',
        active: true,
        lastLogging: new Date().getTime(),
        creationDate: new Date().getTime(),
      },
    ];
    service.getAllUser().subscribe((resp: User[]) => {
      expect(resp.length).toEqual(dataTest.length)
    });
    service.getAllUserById(dataTest[0].email).subscribe((resp: User) => {
      expect(resp.email).toEqual(dataTest[0].email)
    });
    service.createUser(dataTest[0]).subscribe((resp: OperationRequest) => {
      expect(resp.successCodes.length).toEqual(0)
    });
    service.updateUser(dataTest[0]).subscribe((resp: OperationRequest) => {
      expect(resp.successCodes.length).toEqual(0)
    });
    service.deleteUser(dataTest[0].email).subscribe((resp: User[]) => {
      expect(resp.length).toEqual(dataTest.length + 1)
    });
    // const req = httpMock.expectOne(service['urlUserManager']);
    // expect(req.request.method).toBe('GET');
    // req.flush(dataTest);
  });
});
