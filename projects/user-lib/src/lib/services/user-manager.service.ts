import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { OperationRequest, User } from '../entities/user';
import * as uuid from "uuid";

@Injectable({
  providedIn: 'root',
})
export class UserManagerService {
  private urlUserManager: string = 'https://user-manager.dekra.es/';

  private ELEMENT_DATA: User[] = [
    {
      id: uuid.v4(),
      username: 'demo',
      name: 'Demo',
      surnames: 'demo',
      email: 'demo@email.com',
      password: 'demo',
      age: '50',
      active: true,
      lastLogging: new Date().getTime(),
      creationDate: new Date().getTime(),
    },
  ];

  constructor(private httpClient: HttpClient) {}

  getAllUser(): Observable<User[]> {
    const subject: BehaviorSubject<User[]> = new BehaviorSubject<User[]>([]);
    // this.httpClient
    //   .get(this.urlUserManager, { withCredentials: true })
    //   .subscribe(
    //     (values: User[]) => {
    //       subject.next(values);
    //     },
    //     (error) => {
    //       subject.next(error);
    //     },
    //     () => {
    //       subject.complete();
    //     }
    //   );
    subject.next(this.ELEMENT_DATA);
    return subject.asObservable();
  }

  getAllUserById(email: string): Observable<User> {
    const subject: BehaviorSubject<User> = new BehaviorSubject<User>(undefined);
    // this.httpClient
    //   .get(this.urlUserManager + username, { withCredentials: true })
    //   .subscribe(
    //     (values: User[]) => {
    //       subject.next(values);
    //     },
    //     (error) => {
    //       subject.next(error);
    //     },
    //     () => {
    //       subject.complete();
    //     }
    //   );
    subject.next(
      this.ELEMENT_DATA.find((elem: User) => {
        return elem.email === email;
      })
    );
    return subject.asObservable();
  }

  createUser(user: User): Observable<OperationRequest> {
    const subject: BehaviorSubject<OperationRequest> =
      new BehaviorSubject<OperationRequest>(undefined);
    // this.httpClient
    //   .post(this.urlUserManager, user, { withCredentials: true })
    //   .subscribe(
    //     (values: OperationRequest) => {
    //       subject.next(values);
    //     },
    //     (error) => {
    //       subject.next(error);
    //     },
    //     () => {
    //       subject.complete();
    //     }
    //   );
    this.ELEMENT_DATA.push(user);
    subject.next({
      successCodes: [],
      errorCodes: [],
    });
    return subject.asObservable();
  }

  updateUser(user: User): Observable<OperationRequest> {
    const subject: BehaviorSubject<OperationRequest> =
      new BehaviorSubject<OperationRequest>(undefined);
    // this.httpClient
    //   .put(this.urlUserManager, user, { withCredentials: true })
    //   .subscribe(
    //     (values: OperationRequest) => {
    //       subject.next(values);
    //     },
    //     (error) => {
    //       subject.next(error);
    //     },
    //     () => {
    //       subject.complete();
    //     }
    //   );
    const idx = this.ELEMENT_DATA.findIndex((elem: User) => {
      return elem.id === user.id;
    })
    this.ELEMENT_DATA[idx] = user;
    subject.next({
      successCodes: [],
      errorCodes: [],
    });
    return subject.asObservable();
  }

  deleteUser(id: string): Observable<User[]> {
    const subject: BehaviorSubject<User[]> =
      new BehaviorSubject<User[]>([]);
    // this.httpClient
    //   .delete(this.urlUserManager + id, { withCredentials: true })
    //   .subscribe(
    //     (values: OperationRequest) => {
    //       subject.next(values);
    //     },
    //     (error) => {
    //       subject.next(error);
    //     },
    //     () => {
    //       subject.complete();
    //     }
    //   );
    this.ELEMENT_DATA = this.ELEMENT_DATA.filter((elem: User) => {
        return elem.id !== id;
      })
      subject.next(this.ELEMENT_DATA);
    return subject.asObservable();
  }
}
