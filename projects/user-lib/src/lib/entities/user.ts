export class User {
    id: string;
    username: string;
    name: string;
    surnames: string;
    email: string;
    password: string;
    age: string;
    active: boolean;
    lastLogging: number;
    creationDate: number;
}
export class OperationRequest {
    successCodes: string[];
    errorCodes: string[];
}